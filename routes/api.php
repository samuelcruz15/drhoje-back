<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});



Route::namespace('Api')->group(function(){
    //Escola
   Route::get('/home', 'EscolasController@index');
   Route::post('/cadastrarEscola', 'EscolasController@create');
   Route::get('/visualizarEscola/{id}', 'EscolasController@list');
   Route::put('/editarEscola/{id}', 'EscolasController@update');
   Route::delete('/deleteEscola/{id}', 'EscolasController@destroy');
   
   //Turma
   Route::get('/homeTurma', 'TurmasController@index');
   Route::post('/cadastrarTurma', 'TurmasController@create');
   Route::get('/visualizarTurmas/{id}', 'TurmasController@listEscola');
   Route::get('/visualizarTurma/{id}', 'TurmasController@listTurma');
   Route::put('/editarTurma/{id}', 'TurmasController@update');
   Route::delete('/deleteTurma/{id}', 'TurmasController@destroy');

      //Aluno
   Route::get('/homeAluno', 'AlunosController@index');
   Route::post('/cadastrarAluno', 'AlunosController@create');
   Route::get('/visualizarAlunos/{id}', 'AlunosController@listTurma');
   Route::get('/visualizarAluno/{id}', 'AlunosController@listAluno');
   Route::put('/editarAluno/{id}', 'AlunosController@update');
   Route::delete('/deleteAluno/{id}', 'AlunosController@destroy');
   Route::put('/transferirAluno/{id}', 'AlunosController@transferirAluno');
   Route::get('/visualizarAlunosTransferidos', 'AlunosController@listAlunoTransferido');
   Route::put('/vincularAluno/{id}', 'AlunosController@vincularAluno');


});