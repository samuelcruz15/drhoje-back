<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Turma extends Model
{
    public $timestamps = false;
    protected $fillable = ['nome','ano','complemento'];

    public function matriculas()
    {
        return $this->hasMany(Matriculas::class);
    }

    public function escola()
    {
        return $this->belongsTo(Escola::class);
    }

  
    
}