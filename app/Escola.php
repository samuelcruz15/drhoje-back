<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Escola extends Model
{ 
    public $timestamps = false;
    protected $fillable = ['nome'];

    public function turmas()
    {
        return $this->hasMany(Turmas::class);
    }
}