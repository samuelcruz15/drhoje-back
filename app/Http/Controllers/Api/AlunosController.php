<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Aluno;
use App\Matricula;


class AlunosController extends Controller
{
    
       public function index()
    {
        return Aluno::orderBy('nome')->get();
    }

    
    public function create(Request $request)
    { 
        DB::beginTransaction();
        try{  
       $aluno = new Aluno();
       $aluno->nome = $request->aluno['nome'];
       $aluno->data_nascimento = $request->aluno['data_nascimento'];
       $aluno->status = "A";
       $aluno->sexo = $request->aluno['sexo'];
       $aluno->save();

       //Vinculando a turma
        $matricula = new Matricula();
        $matricula->turma_id=$request->aluno['turma_id'];
        $matricula->aluno_id=$aluno->id;
        $matricula->save();
    DB::commit();
       return $response=[
            "erro" => false,
            "messagem" => "aluno cadastrado com sucesso!"
            ];

        }catch(\Exception $erro){
            
            return $response=[
                "erro" => true,
                "messagem" => "aluno não cadastrada !",
                'Details' => $erro
                ];
                DB::rollback();
        }
     
    }

    public function listTurma(int $turmaId){
       
        return $aluno = DB::table('alunos')
            ->join('matriculas', 'alunos.id', '=', 'matriculas.aluno_id')
            ->join('turmas', 'matriculas.turma_id', '=', 'turmas.id')
            ->select('alunos.*', )
            ->where('turmas.id', $turmaId)
            ->orderBy('nome')
            ->get();
         
    }

     public function listAluno(int $alunoId){
        return Aluno::find($alunoId);
         
    }

   
    public function update(Request $request, int $alunoId){

        try{
            $aluno = aluno::find($alunoId);

             $aluno->nome = $request->nome;
             $aluno->data_nascimento = $request->data_nascimento;
             $aluno->sexo = $request->sexo;
             $aluno->status = $request->statusAluno;

            $aluno->save();

            return $response = [
            "erro" => false,
            "mensagem" => "Aluno editado com sucesso!"
        ];

        }catch(\Exception $erro ){
            return  $response = [
            "erro" => true,
            "mensagem" => "Aluno não editado!",
            'Details' => $erro
        ];
        }
    }
     public function destroy(int $alunoId)
    {
        DB::beginTransaction();
         try{
            $matricula = DB::table('matriculas')
                ->where('aluno_id', $alunoId);
            $matricula->delete();

            $aluno = Aluno::find($alunoId);
            $aluno->delete();
         DB::commit();
            return $response = [
        "erro" => false,
        "mensagem" => "Aluno apagada com sucesso!"
         ];

        }catch(\Exception $erro ){
          return $response = [
        "erro" => true,
        "mensagem" => "Erro: Aluno não apagado !",
        'Details' => $erro
         ];
           DB::rollback();
        }
    
    }

    public function listAlunoTransferido()
    {
      $aluno = DB::table('alunos')
                ->where('alunos.status','T')->exists();

                if($aluno==1){
               return  $aluno = DB::table('alunos')
                ->where('alunos.status','T')
                ->get();
                }else{
 return $response = [
        array("id" => ''),
        array("nome" => "Sem alunos desvinculados no momento")
         ];
                }
                
         
    }

    public function transferirAluno(int $alunoId)
    {
        DB::beginTransaction();
         try{
            $matricula = DB::table('matriculas')
                ->where('aluno_id', $alunoId);
            $matricula->delete();

            $aluno = Aluno::find($alunoId);
             $aluno->status='T';
            $aluno->save();
         DB::commit();
            return $response = [
        "erro" => false,
        "mensagem" => "Aluno Transferido com sucesso!"
         ];

        }catch(\Exception $erro ){
          return $response = [
        "erro" => true,
        "mensagem" => "Erro: Aluno não Transferido !",
        'Details' => $erro
         ];
           DB::rollback();
        }
    }

    public function vincularAluno(Request $request, int $alunoId)
    {
        DB::beginTransaction();
         try{
            $matricula = new Matricula();

            $matricula->turma_id = $request->turma_id;
            $matricula->aluno_id = $alunoId;
            $matricula->save();


            $aluno = Aluno::find($alunoId);
            $aluno->status='A';
            $aluno->save();
         DB::commit();
            return $response = [
        "erro" => false,
        "mensagem" => "Aluno Vinculado com sucesso!"
         ];

        }catch(\Exception $erro ){
          return $response = [
        "erro" => true,
        "mensagem" => "Erro: Aluno não Vinculado !",
        'Details' => $erro
         ];
           DB::rollback();
        }
    }
}