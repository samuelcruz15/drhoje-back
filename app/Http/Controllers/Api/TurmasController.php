<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Turma;
use Illuminate\Support\Facades\DB;



class TurmasController extends Controller
{

       public function index()
    {
        return Turma::orderBy('nome')->get();
    }

    
    public function create(Request $request)
    {
        try{  
       $turma = new Turma();
       $turma->escola_id = $request->turma['escola_id'];
       $turma->nome = $request->turma['nome'];
       $turma->complemento = $request->turma['complemento'];
       $turma->ano = $request->turma['ano'];

       $turma->save();

       return $response=[
            "erro" => false,
            "messagem" => "Turma cadastrado com sucesso!"
            ];

        }catch(\Exception $erro){
            
            return $response=[
                "erro" => true,
                "messagem" => "Turma não cadastrada !",
                'Details' => $erro
                ];
        }
     
    }

    public function listEscola(int $escolaId){
       
        return $turma = DB::table('turmas')
            ->join('escolas', 'turmas.escola_id', '=', 'escolas.id')
            ->select('turmas.*', 'escolas.nome as nomeEscola')
            ->where('escolas.id', $escolaId)
            ->orderBy('nome')
            ->get();
         
    }

     public function listTurma(int $turmaId){
       
        return Turma::find($turmaId);
         
    }

   
    public function update(Request $request, int $turmaId){
        try{
            $turma = turma::find($turmaId);

             $turma->nome = $request->nome;
             $turma->complemento = $request->complemento;
             $turma->ano = $request->ano;

            $turma->save();

            return $response = [
            "erro" => false,
            "mensagem" => "Turma editada com sucesso!"
        ];

        }catch(\Exception $erro ){
            return  $response = [
            "erro" => true,
            "mensagem" => "Turma não editada!",
            'Details' => $erro
        ];
        }
    }
     public function destroy(int $turmaId)
    {
         try{
            $turma = Turma::find($turmaId);
            $turma->delete();

            return $response = [
        "erro" => false,
        "mensagem" => "Turma apagada com sucesso!"
         ];

        }catch(\Exception $erro ){
          return $response = [
        "erro" => true,
        "mensagem" => "Erro: Não é possível excluir turmas com vínculos(Alunos) !",
        'Details' => $erro
         ];
        }
    
    }
   
}