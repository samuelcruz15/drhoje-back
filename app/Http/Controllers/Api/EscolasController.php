<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Escola;



class EscolasController extends Controller
{
    
    public function index()
    {
        return Escola::orderBy('nome')->get();
    }

    
    public function create(Request $request)
    {
        try{  
       $escola = new Escola();
       $escola->nome = $request->escola['nome'];

       $escola->save();

       return $response=[
            "erro" => false,
            "messagem" => "Escola cadastrado com sucesso!"
            ];

        }catch(\Exception $erro){
            return $response=[
                "erro" => true,
                "messagem" => "Escola não cadastrada !",
                'Details' => $erro
                ];
        }
     
    }

    public function list(int $escolaId){
       
        return Escola::find($escolaId);
        
    }

   
    public function update(Request $request, int $escolaId){
        try{
            $escola = Escola::find($escolaId);

            $escola->nome = $request->nome;
            $escola->save();

            return $response = [
            "erro" => false,
            "mensagem" => "Escola editada com sucesso!"
        ];

        }catch(\Exception $erro ){
            return  $response = [
            "erro" => true,
            "mensagem" => "Escola não editada!",
            'Details' => $erro
        ];
        }
    }
     public function destroy(int $escolaId)
    {
         try{
            $escola = Escola::find($escolaId);
            $escola->delete();

            return $response = [
        "erro" => false,
        "mensagem" => "Escola apagada com sucesso!"
         ];

        }catch(\Exception $erro ){
          return $response = [
        "erro" => true,
        "mensagem" => "Erro: Não é possível excluir escola com vínculos(Turmas ou Alunos) !",
        'Details' => $erro
         ];
        }
    
    }
   
}