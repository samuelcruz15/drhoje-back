<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Matricula extends Model
{
    public $timestamps = false;
 public function turma()
    {
        return $this->belongsTo(Turma::class);
    }
    
 public function aluno()
    {
        return $this->belongsTo(Aluno::class);
    }

}