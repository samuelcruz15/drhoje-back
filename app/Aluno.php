<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Aluno extends Model
{
    public $timestamps = false;
    protected $fillable = ['nome','data_nascimento','status','sexo'];
    
  public function matriculas()
    {
        return $this->hasMany(Matriculas::class);
    }

}